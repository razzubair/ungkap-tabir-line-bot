UngkapTabir LINE Bot
=================================

This project is a fork from https://github.com/line/line-bot-sdk-python, a Python SDK for LINE Bot.

What's this project about?
***************************

Have you ever wondered what was your group member's last message that they just *unsent*? 
With this LINE Bot, you can show the message that your friend just unsent!

How does it works?
***************************

It works by reading the latest messages in a group, and then shows those latest messages.
It is as simple as that. So, yes, this bot records messages in a group.

What commands does this bot receive?
************************************

Work in progress :)



